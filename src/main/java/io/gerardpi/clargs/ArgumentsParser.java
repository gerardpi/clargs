package io.gerardpi.clargs;

import com.google.common.collect.Iterators;
import com.google.common.collect.PeekingIterator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ArgumentsParser {
    ArgumentsParser() {

    }

    static List<Argument> parseArguments(List<Argument> parameters, String[] args) {
        PeekingIterator<String> argIterator = Iterators.peekingIterator(Arrays.asList(args).iterator());
        List<Argument> arguments = new ArrayList<>();
        while (argIterator.hasNext()) {
            parseArguments(parameters, argIterator, arguments);
        }
        arguments.addAll(determineMissingArgumentErrors(parameters, arguments));
        return Collections.unmodifiableList(arguments);
    }

    private static void parseArguments(List<Argument> parameters, PeekingIterator<String> argIterator, List<Argument> arguments) {
        String arg = argIterator.next();
        for (Argument parameter : parameters) {
            parseArgument(parameter, argIterator, arguments, arg);
        }
    }


    private static void parseArgument(Argument parameter, PeekingIterator<String> argIterator, List<Argument> result, String arg) {
        if (parameter.matchesKey(arg)) {
            if (argIterator.hasNext()) {
                String argPeeked = argIterator.peek();
                if (Argument.isKey(argPeeked)) {
                    if (parameter.getValueType().requiresValue()) {
                        result.add(parameter.withMissingArgumentValueError());
                    }
                    result.add(parameter);
                } else {
                    if (parameter.getValueType().expectsValue()) {
                        result.add(parameter.withValue(argPeeked));
                        argIterator.next();
                    } else {
                        result.add(parameter.withNoArgumentValueExpectedError());
                    }
                }
            } else {
                if (parameter.getValueType().requiresValue()) {
                    result.add(parameter.withMissingArgumentValueError());
                } else {
                    result.add(parameter);
                }
            }
        }
    }

    private static List<Argument> determineMissingArgumentErrors(List<Argument> parameters, List<Argument> argumentsFound) {
        return parameters.stream()
                .filter(parameter -> parameter.isRequired() && argumentsFound.stream().noneMatch(argument -> argument.matches(parameter)))
                .map(Argument::withMissingArgumentError)
                .toList();
    }
}
