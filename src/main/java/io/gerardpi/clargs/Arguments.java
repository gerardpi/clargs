package io.gerardpi.clargs;


import java.util.*;
import java.util.stream.Collectors;

/**
 * This class contains parameters to be used to parse a commaind line.
 */
public class Arguments {
    private final List<Argument> parameters;
    private final List<Argument> arguments;

    private Arguments(List<Argument> parameters, List<Argument> arguments) {
        this.parameters = parameters;
        this.arguments = arguments;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Arguments.class.getSimpleName() + "[", "]")
                .add("parameters=" + parameters)
                .add("arguments=" + arguments)
                .toString();
    }

    /**
     * @return When no command line was parsed, it will produce some information.
     * When a command line was parsed, and no errors were found, it will return a toString representation.
     */
    public String displayValue() {
        return parameters.stream().map(Argument::displayValue).collect(Collectors.joining(System.lineSeparator()));
    }

    public String errorsDisplayValue() {
        return getArgumentsWithErrors().stream().map(argument -> argument.getError().getMessage()).collect(Collectors.joining(System.lineSeparator()));
    }

    /**
     * Use this method to create parameters.
     *
     * @return Arguments that are actually still just parameters, waiting to be filled.
     * See ArgumentsParser parseArguments fill the parameters to be arguments
     */
    public static Arguments create(List<Argument> parameters) {
        return new Arguments(parameters, Collections.emptyList());
    }

    /**
     * @return All the arguments found without errors.
     */
    public List<Argument> get() {
        return arguments.stream().filter(Argument::isSuccess).collect(Collectors.toList());
    }

    public boolean hasErrors() {
        return this.arguments.stream().anyMatch(argument -> !argument.isSuccess());
    }

    public List<Argument> getArgumentsWithErrors() {
        return this.arguments.stream().filter(argument -> !argument.isSuccess()).collect(Collectors.toList());
    }

    public boolean isFilled() {
        return !arguments.isEmpty();
    }

    /**
     * Use this method to parse the command line.
     *
     * @return An Arguments object that contains parameters that are present in the command line.
     */
    public Arguments parseArgs(String[] args) {
        return new Arguments(parameters, ArgumentsParser.parseArguments(this.parameters, args));
    }


    public boolean hasArgument(String argumentKey) {
        return arguments.stream().anyMatch(argument -> argument.matchesKey(argumentKey));
    }

    public String getRequiredValue(String key) {
        return getArgument(key)
                .map(Argument::getValue)
                .orElseThrow(() -> new NoSuchElementException("There is no require value for argument '" + key + "'"));
    }


    public Optional<String> getValue(String key) {
        return getArgument(key)
                .map(Argument::getValue);
    }

    public Optional<Argument> getArgument(String key) {
        return arguments.stream()
                .filter(argument -> argument.matchesKey(key))
                .findAny();
    }

    public Argument getRequiredArgument(String key) {
        return getArgument(key).orElseThrow(() -> new NoSuchElementException("There is no required argument '" + key + "'"));
    }

    public Optional<Argument> getParameter(String key) {
        return parameters.stream()
                .filter(parameter -> parameter.matchesKey(key))
                .findAny();
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private final List<Argument> parameters;

        Builder() {
            this.parameters = new ArrayList<>();
        }

        public Builder add(Argument parameter) {
            parameters.add(parameter);
            return this;
        }

        public Arguments build() {
            return Arguments.create(parameters);
        }
    }
}
