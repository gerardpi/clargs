package io.gerardpi.clargs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TesterApp {
    private static final Logger LOG = LoggerFactory.getLogger(ArgumentsTest.class);
    public static void main(String[] args) {
        Arguments arguments = Arguments.builder()
                .add(Argument.builder()
                        .setKeys("k", "kaas")
                        .setRequired(true)
                        .setValueRequired().build())
                .add(Argument.builder()
                        .setKeys("w", "worst")
                        .setRequired(true)
                        .setValueOptional().build())
                .add(Argument.builder()
                        .setKeys("t", "topping")
                        .setRequired(false)
                        .setValueOptional().build())
                .build().parseArgs(args);
        LOG.info("{}", arguments.displayValue());
        LOG.info("There are " + arguments.getArgumentsWithErrors().size() + " arguments with errors");
        arguments.getArgumentsWithErrors().forEach(argument -> System.out.println(argument.getError().getMessage()));
        LOG.info("There are " + arguments.get().size() + " arguments");
        arguments.get().forEach(argument -> LOG.info("argument: " + argument));
    }
}
