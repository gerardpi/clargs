package io.gerardpi.clargs;

import com.google.common.collect.ImmutableList;
import com.tngtech.jgiven.Stage;
import com.tngtech.jgiven.annotation.Quoted;
import com.tngtech.jgiven.junit5.SimpleScenarioTest;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class ArgumentsTest extends SimpleScenarioTest<ArgumentsTest.State> {


    @Test
    void happy_flow() {
        given().expected_required_argument_with_short_key_$_and_long_key_$_that_has_a_required_value("k", "kaas")
                .and().expected_required_argument_with_short_key_$_and_long_key_$_that_has_an_optional_value("w", "worst")
                .and().expected_required_argument_with_short_key_$_and_long_key_$_that_has_no_value("t", "topping")
                .and().expected_optional_argument_$_that_has_no_value("s", "sauce")
                .and().the_display_value_is_$("""
                -k or --kaas; required; value required; description: '<none>'
                -w or --worst; required; value optional; description: '<none>'
                -t or --topping; required; no value; description: '<none>'
                -s or --sauce; not required; no value; description: '<none>'
                """);
        when().command_line_arguments_$_are_parsed(ImmutableList.of("-k", "edammer", "-w", "knack", "-t"));
        then().$_arguments_were_found(3)
                .and().$_errors_were_found(0)
                .and().an_argument_with_key_$_and_required_value_$_is_found("kaas", "edammer")
                .and().an_argument_with_key_$_and_optional_value_$_is_found("w", "knack")
                .and().an_argument_with_key_$_and_no_value_is_found("t");
    }

    @Test
    void error_required_argument_missing() {
        given().expected_required_argument_with_short_key_$_and_long_key_$_that_has_a_required_value("k", "kaas")
                .and().expected_required_argument_with_short_key_$_and_long_key_$_that_has_an_optional_value("w", "worst")
                .and().expected_required_argument_with_short_key_$_and_long_key_$_that_has_no_value("t", "topping");
        when().command_line_arguments_$_are_parsed(ImmutableList.of("-k", "edammer", "-w", "knack"));
        then().$_arguments_were_found(2)
                .and().$_errors_were_found(1)
                .and().an_argument_with_key_$_and_required_value_$_is_found("kaas", "edammer")
                .and().an_argument_with_key_$_and_optional_value_$_is_found("worst", "knack")
                .and().an_argument_with_key_$_and_with_error_message_$_is_found("t",
                        "Missing argument -t or --topping; required; no value; description: '<none>'")
                .and().the_errors_display_value_is_$("""
                        Missing argument -t or --topping; required; no value; description: '<none>'
                        """);
    }

    private String getText(String resourceName) {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(Objects.requireNonNull(getClass().getResourceAsStream(resourceName), "Can not read file " + resourceName), StandardCharsets.UTF_8))) {
            return reader.lines().collect(Collectors.joining(System.lineSeparator()));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    static class State extends Stage<State> {
        private final Arguments.Builder argumentsBuilder = Arguments.builder();
        private Arguments arguments;


        State expected_required_argument_with_short_key_$_and_long_key_$_that_has_a_required_value(@Quoted String shortKey, @Quoted String longKey) {
            argumentsBuilder.add(Argument.builder().setRequired(true).setKeys(shortKey, longKey).setValueRequired().build());
            return self();
        }

        State expected_required_argument_with_short_key_$_and_long_key_$_that_has_an_optional_value(@Quoted String shortKey, @Quoted String longKey) {
            argumentsBuilder.add(Argument.builder().setRequired(true).setKeys(shortKey, longKey).setValueOptional().build());
            return self();
        }

        State expected_required_argument_with_short_key_$_and_long_key_$_that_has_no_value(@Quoted String shortKey, @Quoted String longKey) {
            argumentsBuilder.add(Argument.builder().setRequired(true).setKeys(shortKey, longKey).build());
            return self();
        }

        State expected_optional_argument_$_that_has_no_value(@Quoted String shortKey, @Quoted String longKey) {
            argumentsBuilder.add(Argument.builder().setRequired(false).setKeys(shortKey, longKey).build());
            return self();
        }

        State command_line_arguments_$_are_parsed(@Quoted List<String> commandLineArguments) {
            String[] args = commandLineArguments.toArray(new String[0]);
            Arguments unparsedArguments = argumentsBuilder.build();
            assertThat(unparsedArguments.isFilled()).isFalse();
            this.arguments = unparsedArguments.parseArgs(args);
            assertThat(arguments.isFilled()).isTrue();
            return self();
        }

        State $_errors_were_found(int expectedErrorCount) {
            assertThat(arguments.getArgumentsWithErrors().size()).isEqualTo(expectedErrorCount);
            return self();
        }

        State an_argument_with_key_$_and_required_value_$_is_found(@Quoted String key, @Quoted String expectedValue) {
            assertThat(arguments.hasArgument(key)).isTrue();
            assertThat(arguments.getRequiredArgument(key).containsValue()).isTrue();
            assertThat(arguments.getRequiredValue(key)).isEqualTo(expectedValue);
            return self();
        }

        State an_argument_with_key_$_and_optional_value_$_is_found(@Quoted String key, @Quoted String expectedValue) {
            assertThat(arguments.hasArgument(key)).isTrue();
            assertThat(arguments.getArgument(key)).isPresent();
            assertThat(arguments.getArgument(key).get().containsValue()).isTrue();
            assertThat(arguments.getValue(key)).isNotEmpty();
            assertThat(arguments.getValue(key)).hasValue(expectedValue);
            return self();
        }

        State an_argument_with_key_$_and_no_value_is_found(@Quoted String key) {
            assertThat(arguments.hasArgument(key)).isTrue();
            assertThat(arguments.getArgument(key).get().isSuccess()).isTrue();
            return self();
        }

        State an_argument_with_key_$_and_with_error_message_$_is_found(@Quoted String key, @Quoted String expectedErrorMessage) {
            assertThat(arguments.hasArgument(key)).isTrue();
            assertThat(arguments.getArgument(key).get().isSuccess()).isFalse();
            assertThat(arguments.getArgument(key).get().getError().getMessage()).isEqualTo(expectedErrorMessage);
            return self();
        }

        State $_arguments_were_found(int expectedArgumentsCount) {
            assertThat(arguments.get().size()).isEqualTo(expectedArgumentsCount);
            return self();
        }

        State the_display_value_is_$(@Quoted String expectedDisplayValue) {
            assertThat(argumentsBuilder.build().displayValue()).isEqualToNormalizingWhitespace(expectedDisplayValue);
            return self();
        }

        State the_errors_display_value_is_$(@Quoted String expectedDisplayValue) {
            assertThat(arguments.errorsDisplayValue()).isEqualToNormalizingWhitespace(expectedDisplayValue);
            return self();
        }
    }
}
