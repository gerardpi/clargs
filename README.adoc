= Clargs

== Introduction

This is yet another Command Line Argument parser.
For details, read the tests, and the test application undder `src/main/test/`.

== Requires

Java 17 or higher.

== Gitlab specifics

=== Java Spring template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).
Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).

=== CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).
If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.

=== Pipeline status

image:https://gitlab.com/gerardpi/clargs/badges/master/pipeline.svg[link="https://gitlab.com/gerardpi/clargs/-/commits/master",title="pipeline status"]

image:https://gitlab.com/gerardpi/clargs/badges/master/coverage.svg[link="https://gitlab.com/gerardpi/clargs/-/commits/master",title="coverage report"]

image:https://gitlab.com/gerardpi/clargs/-/badges/release.svg[link="https://gitlab.com/gerardpi/clargs/-/releases",title="Latest Release"]
